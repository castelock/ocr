import pytesseract
from PIL import Image, ImageEnhance, ImageFilter

# Load the image using Pillow
image_path = '/home/juanje/Documents/Recetas/Receta_guisantes_jamón.jpg'
image = Image.open(image_path)

# Preprocessing: Enhance contrast and reduce noise
#enhancer = ImageEnhance.Contrast(image)
#image = enhancer.enhance(2.0)  # Increase contrast

#image = image.filter(ImageFilter.SMOOTH_MORE)  # Reduce noise

# Perform OCR on the image
# Perform OCR on the preprocessed image
config = '--psm 6 --oem 3 -c tessedit_char_whitelist=0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
text = pytesseract.image_to_string(image)

# Set the path to the output text file
output_file_path = 'output_guisantes.txt'

# Write the extracted text to the output file
with open(output_file_path, 'w') as file:
    file.write(text)

print("Text extracted and saved to:", output_file_path)